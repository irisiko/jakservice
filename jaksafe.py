import ConfigParser
import os
import MySQLdb

global_conf_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'global_conf.cfg')

def get_database_connection(ip_address,user,paswd,database):
    con = MySQLdb.connect(ip_address,user,paswd,database)
    return con

## Defining the parser
global_conf_parser = ConfigParser.SafeConfigParser()
global_conf_parser.read(global_conf_file)

## Dims configuration
dims_url_base = global_conf_parser.get('dims_conf','url_dims')

## Database configuration
database_url_address = global_conf_parser.get('database_configuration','url_address')
user = global_conf_parser.get('database_configuration','user')
paswd = global_conf_parser.get('database_configuration','paswd')
database_name = global_conf_parser.get('database_configuration','database_name')

## QGIS installation path
qgis_install_path = global_conf_parser.get('qgis_conf','qgis_install_path')

## Open database connection
db_con = get_database_connection(database_url_address,user,paswd,database_name)
