__AUTHOR__= 'FARIZA DIAN PRASETYO'

from jaksafe import *

import pandas as pd
import numpy as np
import os

from header_config_variable import *

def compile_flood_event(df_all_units):
    df_compile = df_all_units[[header_id_unit,header_depth]]
    grb = df_compile.groupby(header_id_unit)
    df1 = grb.aggregate(np.mean).rename(columns = {header_depth:'mean_depth'})
    df1['mean_depth'] = df1['mean_depth']
    df1['count'] = grb.aggregate(len).rename(columns = {header_depth:'count'})
    df1['duration'] = (df1['count'] * 6)/24.
    return df1

def compiling_hazard_fl(df_all_units,hazard_config_file):
    df_compiled = compile_flood_event(df_all_units)
    df_compiled = mapping_compiled_data_to_hazard_class(df_compiled,hazard_config_file)
    df_compiled.to_csv('compiled_event.csv')
    return df_compiled

def compiling_hazard_fl_in_folder(df_all_units,hazard_config_file,base_folder_output,t0,t1):
    t1.set_time_format(glob_folder_format)
    t0.set_time_format(glob_folder_format)

    ## ## /output/hazard/20150210055959_20150210115959/
    output_directory = base_folder_output + '/hazard/' + t0.formattedTime() + '_' + t1.formattedTime() + '/'

    if not os.path.exists(output_directory):
        os.makedirs(output_directory)

    df_compiled = compile_flood_event(df_all_units)
    df_compiled = mapping_compiled_data_to_hazard_class(df_compiled,hazard_config_file)
    df_compiled.to_csv(output_directory + 'compiled_event.csv')
    return df_compiled

def mapping_compiled_data_to_hazard_class(df_compiled,hazard_class_file):
    df_config_hazard = pd.read_csv(hazard_class_file)
    df_compiled['kelas'] = df_compiled.apply(lambda row: classify_hazard_class(row['mean_depth'],row['duration'],df_config_hazard), axis = 1)
    return df_compiled

def classify_hazard_class(mean_depth,duration,df_config_hazard):
    kelas = None
    for idx,row in df_config_hazard.iterrows():
        if mean_depth >= row['kedalaman_bawah'] and mean_depth <= row['kedalaman_atas']:
            if duration >= row['durasi_bawah'] and duration <= row['durasi_atas']:
                kelas = row['kelas']
                break
    return kelas
