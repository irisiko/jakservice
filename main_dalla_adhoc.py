__AUTHOR__= 'FARIZA DIAN PRASETYO'

from jaksafe import global_conf_parser
from jaksafe import qgis_install_path

from config_folder import input_folder
from config_folder import hazard_kelas_folder
from config_folder import input_boundary_folder
from config_folder import input_exposure_folder
from config_folder import input_exposure_shapefile_folder
from config_folder import adhoc_output_folder

from auto_preprocessing.preprocess_fl_report import *
from auto_preprocessing.auto_calc_function import *

from impact_analysis.hazard_compilation_function import *
from impact_analysis.hazard_analysis_class import HazardLayer
from impact_analysis.exposure_analysis_class import ExposureLayer

import Time as t
import requests
import sys
import datetime

# Package QGIS
from qgis.core import *
import qgis.utils
from PyQt4.QtCore import *

#input from data dims and database configuration
base_dims_url = global_conf_parser.get('dims_conf','url_dims')
table_name_event = global_conf_parser.get('database_configuration','table_name_event')
table_raw_name_event = global_conf_parser.get('database_configuration','table_raw_name_event')
table_name_autocalc = global_conf_parser.get('database_configuration','table_name_autocalc')

report_RT_name = global_conf_parser.get('file_output','output_rt_report')
report_RW_name = global_conf_parser.get('file_output','output_rw_report')

#input kelas hazard
kelas_hazard_config_name = global_conf_parser.get('file_input','input_kelas')
kelas_hazard_file = hazard_kelas_folder + '/' + kelas_hazard_config_name

#input layer for hazard
boundary_name = global_conf_parser.get('file_input','input_boundary_layer')
input_boundary_layer_file = input_boundary_folder + '/' + boundary_name

#Output layer for hazard
output_hazard_layer_name = global_conf_parser.get('file_output','output_hazard')

#input layer for building exposure
building_exposure_layer_name = global_conf_parser.get('file_input','input_building_exposure')
input_building_exposure_layer_file = input_exposure_shapefile_folder + '/' + building_exposure_layer_name

#output layer for building exposure
output_building_exposure_layer_name = global_conf_parser.get('file_output','output_building_exposure')

#input layer for road exposure
road_exposure_layer_name = global_conf_parser.get('file_input','input_road_exposure')
input_road_exposure_layer_file = input_exposure_shapefile_folder + '/' + road_exposure_layer_name

#output layer for building exposure
output_road_exposure_layer_name = global_conf_parser.get('file_output','output_road_exposure')

max_observe = 2
base_folder_output = adhoc_output_folder


def calculate_impact_function(df_fl_event,t0,t1):
    try:
        df_all_units = df_fl_event
        ## create fl report
        create_fl_report(df_all_units,t.Time(t1),report_RT_name,report_RW_name,base_folder_output)
        ## Compiling hazard report
        #df_compiled = compiling_hazard_fl(df_all_units,kelas_hazard_file)

        df_compiled = compiling_hazard_fl_in_folder(df_all_units,kelas_hazard_file,base_folder_output,t.Time(t0),t.Time(t1))

        # QgsApplication.setPrefixPath(qgis_install_path, True)
        # QgsApplication.initQgis()

        obj_hazardlayer = HazardLayer(input_boundary_layer_file,kelas_hazard_file,output_hazard_layer_name,base_folder_output,t.Time(t0),t.Time(t1))
        hazard_fl_layer = obj_hazardlayer.create_hazard_shp(df_compiled)

        ## Creating building impact-exposure layer
        obj_buildingExposure = ExposureLayer(input_building_exposure_layer_file,base_folder_output,'building',t.Time(t0),t.Time(t1))
        obj_buildingExposure.set_building_exposure_layer_output(output_building_exposure_layer_name)
        print "Intersecting exposure building with hazard ...."
        obj_buildingExposure.intersect_building_exposure_with_hazard(hazard_fl_layer)

        obj_roadExposure = ExposureLayer(input_road_exposure_layer_file,base_folder_output,'road',t.Time(t0),t.Time(t1))
        obj_roadExposure.create_exposure_road_layer(output_road_exposure_layer_name,hazard_fl_layer)
        print "Intersecting exposure road with hazard ...."
        obj_roadExposure.intersect_road_exposure_with_hazard(hazard_fl_layer)

        # QgsApplication.exitQgis()

    except Exception,e:
        print e
        sys.exit(1)
    return obj_buildingExposure,obj_roadExposure

def create_summary_auto_calculation(t0,t1,db_con,table_name_autocalc):
    table_name = table_name_autocalc
    print 'wakakco'
    print table_name
    sql_dump = "INSERT INTO "+table_name+ \
               " (t0,t1,damage,loss) VALUES ('%s','%s',NULL,NULL)"%(t0,t1)

    # prepare a cursor object using cursor() method
    cursor = db_con.cursor()
    cursor.execute(sql_dump)
    db_con.commit()
    last_row_id = cursor.lastrowid
    return last_row_id


def main_adhoc_impact_analysis(t0,t1,db_con):
    QgsApplication.setPrefixPath(qgis_install_path, True)
    QgsApplication.initQgis()

    print "Fetching data from fl_event..."
    print t0
    print t1

    df_fl_event = get_fl_event(db_con,table_name_event,t0,t1)

    if not df_fl_event.empty:

        ## preprocessing df_fl_event
        df_all_units = preprocessing_the_hazard_data(df_fl_event)

        ## calculate impact function
        obj_buildingExposure,obj_roadExposure = calculate_impact_function(df_fl_event,t0,t1)

        print "End of impact analysis service ..."
    else:
        print "There is no Flood event from %s until %s"%(t0,t1)

    return base_folder_output
